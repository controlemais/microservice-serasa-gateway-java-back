package br.com.serasa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class MicroserviceSerasaGatewayJavaBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceSerasaGatewayJavaBackApplication.class, args);
	}
}
