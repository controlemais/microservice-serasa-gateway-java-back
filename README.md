#Projeto Microservice Gateway

Projeto resosánvel or redirecioar as rotas dos serviços rest, e ainda realizar autenticação do sistema em geral.

Como é possível ver no arquivo application.yml, a aplicação gateway faz uso da porta 8080 e redireciona para outros aplicações com portas diferentes.

    server:
        port: 8080
        zuul:
          sensitiveHeaders: Cookie,Set-Cookie
          routes:
            serasa-a-cliente:
              path: /cliente/**
              url: http://localhost:8081/cliente
            serasa-a-divida:
              path: /divida/**
              url: http://localhost:8081/divida
            serasa-b-scorecredito:
              path: /scorecredito/**
              url: http://localhost:8082/scorecredito
            authenticate:
              path: /authenticate/**
              url: http://localhost:8084/api/authenticate
              
Após executar os arquivos docker-compode.yml das aplicações `microservice-serasa-a-java-back` e `microservice-serasa-b-java-back`, afim de criar as imagens necessárias para uso dos bancos de dados, será possivel rodar todas as aplicações e verificar o funcionamento das aplicações.

Foi adicionado o arquivo exportado do Postman `Teste Serasa.postman_collection.json` com os testes das apis disponibilizado nos sistemas.

Note que todas as APIs fazem uso de um tocken de autenticação que foi retornado pelo serviço de login da API de Autenticação `microservice-serasa-authentication-back`.  
